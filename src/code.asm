org $8000

; Define our string to print
string:
     db 'Hello, world!',13

start:
     ; Define some ROM routines
     CHANOPEN  EQU $1601
     PRINT     EQU $203C
     CLS       EQU $0D6B
   
     call CLS            ; clear the screen.

     ld   a, 2           ; load 2 (Upper screen) into the accumulator.
     call CHANOPEN       ; call channel open to switch to that output channel. 

     ld   de, string     ; load into de the address of the text to print.
     ld   bc, 13         ; load into bc the length of the string.
     call PRINT          ; call the print routine.

     ; Return to the operating system
     ret

end start 


