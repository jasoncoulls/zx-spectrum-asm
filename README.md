# README

## What is this?
This is the accompanying code to VLOG 249.

## How to install?
1. Install VS Code.,
2. Install the ZX Spectrum Extension (SpectNetVsc)
3. Install Node, because there’s a bunch of hoops you need to go through when compiling Spectrum assembly.
4. npm install -g yo
5. npm install -g generator-zxspectrum
6. yo zxspectrum
    1. Fill in the details.
7. Open folder in VS Code.
8. Program in an asm file your code.
9. Download the PASMO compiler and set it up.
10. Compile the asm file using pasmo into a .tap file.
11. Load the .tap file into an emulator like Fuse.

## PASMO Command
To compile, use this from the src folder.  It will overwrite any existing tap file.
```
pasmo --tapbas code.z80asm test.tap
```
